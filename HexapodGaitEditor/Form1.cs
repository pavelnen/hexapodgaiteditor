﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HexapodGaitEditor
{
    public partial class Form1 : Form
    {
        public static Map field = new Map();

        bool IsClicked = false;
        int X = 0;
        int Y = 0;
        int X1 = 0;
        int Y1 = 0;

        public Form1()
        {
            InitializeComponent();
            
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Загрузился";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            field.reDraw(pictureBox1);
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            IsClicked = true;
            X = e.X;
            Y = e.Y;

            int col = Y / 30;
            int row = X / 30;

            Map.map[col, row] = 1;

            field.reDrawOne(pictureBox1, col, row);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (IsClicked)
            {
                X1 = e.X;
                Y1 = e.Y;

                int col = Y / 30;
                int row = X1 / 30;

                Map.map[col, row] = 1;

                field.reDrawOne(pictureBox1, col, row);
                //pictureBox1.Invalidate();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            IsClicked = false;
        }
    }
}
