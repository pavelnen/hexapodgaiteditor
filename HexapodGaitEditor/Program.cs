﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace HexapodGaitEditor
{
    public class Map
    {
        public static int[,] map;
        static int sX, sY; // размер карты
        static Bitmap[] sprites = new Bitmap[2];
        static int sprSX, sprSY; // размер спрайта

        public Map() {
            sX = 6;
            sY = 22;
            sprites[0] = new Bitmap(@"C:\Users\Павел\source\repos\HexapodGaitEditor\HexapodGaitEditor\sqw.bmp", true);
            sprites[1] = new Bitmap(@"C:\Users\Павел\source\repos\HexapodGaitEditor\HexapodGaitEditor\sq.bmp", true);
            map = new int[sX, sY];
            for (int i = 0; i < sX; i++)
            {
                for (int j = 0; j < sY; j++)
                {
                    map[i, j] = 0;
                }
            }
            sprSX = 30;
            sprSY = 30;
            
        }

        public void reDraw(PictureBox pbox)
        {
            //pictureBox1 - целевой PictureBox
            Graphics g = pbox.CreateGraphics();
            Monitor.Enter(g);
            for (int x = 0; x < sX; x++)
                for (int y = 0; y < sY; y++)
                    g.DrawImage(sprites[map[x, y]], y * sprSX, x * sprSY);
            Monitor.Exit(g);
        }

        public void reDrawOne(PictureBox pbox, int x , int y)
        {
            //pictureBox1 - целевой PictureBox
            Graphics g = pbox.CreateGraphics();
            Monitor.Enter(g);
            g.DrawImage(sprites[map[x, y]], y * sprSX, x * sprSY);
            Monitor.Exit(g);
        }
    }
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
